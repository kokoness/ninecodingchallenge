﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using NineCodingChallenge.Models;

namespace NineCodingChallenge.Controllers
{
    public class NineCodingChallengeController : ApiController
    {
        private const string Payload = "payload";
        private const string InvalidJson = "Could not decode request: JSON parsing failed";

        public HttpResponseMessage Post([FromBody]JToken jsonBody)
        {
            try
            {
                var payload = jsonBody[Payload];

                var episodes = payload.ToObject<IList<Episode>>();
                var shows = GetShowsWithAnEpisode(episodes, true);

                HttpResponseMessage httpResponseMessage = GetHttpResponseMessage(shows);
                return httpResponseMessage;
            }

            catch (NullReferenceException)
            {
                HttpResponseMessage httpResponseMessage = GetHttpResponseMessage(InvalidJson, HttpStatusCode.BadRequest);
                return httpResponseMessage;
            }

            catch (Exception)
            {
                //catch other exceptions here and change status code if needed
                HttpResponseMessage httpResponseMessage = GetHttpResponseMessage(InvalidJson, HttpStatusCode.BadRequest);
                return httpResponseMessage;
            }
        }

        private IList<Show> GetShowsWithAnEpisode(IList<Episode> episodes, bool drm)
        {
            var shows = new List<Show>();

            foreach (Episode episode in episodes)
            {
                if (episode.EpisodeCount > 0 && episode.DRM == drm)
                {
                    var show = new Show();
                    show.Image = episode.Image.ShowImage;
                    show.Slug = episode.Slug;
                    show.Title = episode.Title;

                    shows.Add(show);
                }
            }

            return shows;
        }

        private HttpResponseMessage GetHttpResponseMessage(IList<Show> shows)
        {
            var response = new ShowResponse(shows);
            JToken json = JsonContent.ConvertToJson(response);

            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage.Content = new JsonContent(json);

            return httpResponseMessage;
        }

        private HttpResponseMessage GetHttpResponseMessage(string errorMessage, HttpStatusCode statusCode)
        {
            var errorResponse = new ErrorResponse(errorMessage);
            JToken json = JsonContent.ConvertToJson(errorResponse);

            HttpResponseMessage httpResponseMessage = new HttpResponseMessage(statusCode);
            httpResponseMessage.Content = new JsonContent(json);

            return httpResponseMessage;
        }
    }
}
