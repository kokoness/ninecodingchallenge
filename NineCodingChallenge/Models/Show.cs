﻿namespace NineCodingChallenge.Models
{
    public class Show
    {
        public string Image { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}