﻿using System.Collections.Generic;

namespace NineCodingChallenge.Models
{
    public class Episode
    {
        public string Country { get; set; }
        public string Description { get; set; }
        public bool DRM { get; set; }
        public int EpisodeCount { get; set; }
        public string Genre { get; set; }
        public Image Image { get; set; }
        public string Language { get; set; }
        public NextEpisode NextEpisode { get; set; }
        public string PrimaryColour { get; set; }
        public IList<Seasons> Seasons { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TvChannel { get; set; }
    }
}