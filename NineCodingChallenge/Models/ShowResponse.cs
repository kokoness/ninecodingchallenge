﻿using System.Collections.Generic;

namespace NineCodingChallenge.Models
{
    public class ShowResponse
    {
        public ShowResponse(IList<Show> response)
        {
            Response = response;
        }

        public IList<Show> Response { get; set; }

    }
}